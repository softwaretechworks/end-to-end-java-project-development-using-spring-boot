package com.softwaretechworks.customerdal;

import com.softwaretechworks.customerdal.entities.Customer;
import com.softwaretechworks.customerdal.repositories.CustomerRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerdalApplicationTests {

    @Autowired
    private CustomerRepository customerRepository;

    @Test
    public void testCreateCustomer() {

        Customer customer = new Customer();

        customer.setName("John");
        customer.setEmail("john@john.com");

        customer = customerRepository.save(customer);

    }

    @Test
    public void testFindCustomerById() {

        Optional<Customer> customerOptional = customerRepository.findById(1l);

        if(customerOptional.isPresent()) {

            Customer customer = customerOptional.get();
            System.out.println(customer);

        }

    }

    @Test
    public void testUpdateCustomer() {

        // Create
        Customer customer;

        customer = new Customer();

        customer.setName("John");
        customer.setEmail("john@john.com");

        customer = customerRepository.save(customer);

        System.out.println(customer + " Created");

        // Find
        Optional<Customer> customerOptional = customerRepository.findById(customer.getId());

        if(customerOptional.isPresent()) {

            customer = customerOptional.get();
            System.out.println(customer);

        }

        customer.setName("John1");
        customer.setEmail("john1@john.com");

        customer = customerRepository.save(customer);

        System.out.println(customer + " Updated");

    }

    @Test
    public void testDeleteCustomer() {

        // Create
        Customer customer;

        customer = new Customer();

        customer.setName("John");
        customer.setEmail("john@john.com");

        customer = customerRepository.save(customer);

        System.out.println(customer + " Created");


        // Find
        Optional<Customer> customerOptional = customerRepository.findById(customer.getId());

        if(customerOptional.isPresent()) {

            customer = customerOptional.get();
            System.out.println(customer);

        }

        customerRepository.delete(customer);

        System.out.println(customer + " Deleted");

    }


}
