package com.softwaretechworks.vendorweb.repositories;

import com.softwaretechworks.vendorweb.entities.Vendor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VendorRepository extends JpaRepository<Vendor, Integer> {
}
