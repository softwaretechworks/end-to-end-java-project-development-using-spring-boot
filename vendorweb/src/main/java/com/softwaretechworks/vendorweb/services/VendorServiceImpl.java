package com.softwaretechworks.vendorweb.services;

import com.softwaretechworks.vendorweb.entities.Vendor;
import com.softwaretechworks.vendorweb.repositories.VendorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class VendorServiceImpl implements VendorService {

    @Autowired
    private VendorRepository repository;

    @Override
    public Vendor saveVendor(Vendor vendor) {
        return repository.save(vendor);
    }

    @Override
    public Vendor updateVendor(Vendor vendor) {
        return repository.save(vendor);
    }

    @Override
    public void deleteVendor(Vendor vendor) {
        repository.delete(vendor);
    }

    @Override
    public Vendor getVendorById(int id) {

        Vendor vendor;

        Optional<Vendor> vendorOptional = repository.findById(id);

        if(vendorOptional.isPresent()) {
            vendor = vendorOptional.get();
        } else {
            vendor = null;
        }

        return vendor;

    }

    @Override
    public List<Vendor> getAllVendors() {
        return repository.findAll();
    }
}
